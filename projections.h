#include <stdio.h>
#include <math.h>

#define MICROSTEP_FACTOR 720

typedef struct {
	double x, y;
} Point;

Point scale(Point v, double f) {
	return (Point){ .x = v.x*f, .y = v.y*f };
}

Point add(Point v1, Point v2) {
	return (Point){ .x = v1.x + v2.x, .y = v1.y + v2.y };
}
Point sub(Point v1, Point v2) {
	return (Point){ .x = v1.x - v2.x, .y = v1.y - v2.y };
}

double dot(Point v1, Point v2)
{
	return v1.x * v2.x + v1.y * v2.y;
}

double distance(Point from, Point to)
{
	double dx = to.x - from.x;
	double dy = to.y - from.y;
	return sqrt(dx*dx + dy*dy);
}

Point direction(Point from, Point to)
{
	double dx = to.x - from.x;
	double dy = to.y - from.y;
	double l  = sqrt(dx*dx + dy*dy);
	return (Point) { .x = dx/l, .y = dy/l };
}

Point q(Point v1, Point v2, double t)
{
	double tx = 1.0 - t;
	Point pA = scale(v1,      tx * tx * tx);
	Point pB = scale(v1,  3 * tx * tx *  t);
	Point pC = scale(v2,  3 * tx *  t *  t);
	Point pD = scale(v2,       t *  t *  t);
	return add(add(add(pA, pB), pC), pD);
}

static Point BezierII(int degree, Point *V, double t)
{
    int 	i, j;
    Point 	Q, Vtemp[4];

    /* Copy array	*/
    for (i = 0; i <= degree; i++) {
		Vtemp[i] = V[i];
    }

    /* Triangle computation	*/
    for (i = 1; i <= degree; i++) {
		for (j = 0; j <= degree-i; j++) {
	    	Vtemp[j].x = (1.0 - t) * Vtemp[j].x + t * Vtemp[j+1].x;
	    	Vtemp[j].y = (1.0 - t) * Vtemp[j].y + t * Vtemp[j+1].y;
		}
    }

    Q = Vtemp[0];
    return Q;
}

static double NewtonRaphsonRootFind(Point *Q, Point P, double u)
{
    double 		numerator, denominator;
    Point 		Q1[3], Q2[2];
    Point		Q_u, Q1_u, Q2_u;
    int 		i;

    /* Compute Q(u)	*/
    Q_u = BezierII(3, Q, u);

    /* Generate control vertices for Q'	*/
    for (i = 0; i <= 2; i++) {
		Q1[i].x = (Q[i+1].x - Q[i].x) * 3.0;
		Q1[i].y = (Q[i+1].y - Q[i].y) * 3.0;
    }

    /* Generate control vertices for Q'' */
    for (i = 0; i <= 1; i++) {
		Q2[i].x = (Q1[i+1].x - Q1[i].x) * 2.0;
		Q2[i].y = (Q1[i+1].y - Q1[i].y) * 2.0;
    }

    /* Compute Q'(u) and Q''(u)	*/
    Q1_u = BezierII(2, Q1, u);
    Q2_u = BezierII(1, Q2, u);

    /* Compute f(u)/f'(u) */
    numerator = (Q_u.x - P.x) * (Q1_u.x) + (Q_u.y - P.y) * (Q1_u.y);
    denominator = (Q1_u.x) * (Q1_u.x) + (Q1_u.y) * (Q1_u.y) +
		      	  (Q_u.x - P.x) * (Q2_u.x) + (Q_u.y - P.y) * (Q2_u.y);

    /* u = u - f(u)/f'(u) */
    if (denominator == 0.0f)
		return u;
	else
		return u - (numerator/denominator);
}

static void reparameterize(Point *points, int count, double *offsets, Point *bezier)
{
    for (int k = 0; k < count; k++) {
		offsets[k] = NewtonRaphsonRootFind(bezier, points[k], offsets[k]);
    }
}


void generateBezier(Point *points, int count, Point l_tangent, Point r_tangent, double *offsets, Point *bezier)
{
	Point a0, a1, tmp;
	int i;
	double epsilon, segment_len, u, ux, alpha_l, alpha_r;

	Point start_point = points[0];
	Point end_point = points[count];

    //Create the C and X matrices
    Point C0 = {0}, C1 = {0}, X = {0};

	for (i = 0; i < count; i++) {
		u = offsets[i];
		ux = 1 - u;
		a0 = scale(l_tangent,  3 * u  * ux*ux);
		a1 = scale(r_tangent, 3 * ux * u*u);

		C0.x += dot(a0, a0);
		C0.y += dot(a0, a1);
		C1.x += dot(a0, a1);
		C1.y += dot(a1, a1);

		tmp = sub(points[i], q(start_point, end_point, u));

		X.x += dot(a0, tmp);
		X.y += dot(a1, tmp);
	}

    double det_C0_C1 = (C0.x * C1.y) - (C1.x * C0.y);
    double det_C0_X  = (C0.x * X.y ) - (C1.x * X.x );
    double det_X_C1  = (X.x  * C1.y) - (X.y  * C0.y);
    alpha_l = det_C0_C1 == 0 ? 0 : det_X_C1 / det_C0_C1;
    alpha_r = det_C0_C1 == 0 ? 0 : det_C0_X / det_C0_C1;

    segment_len = distance(start_point, end_point);
    epsilon = 1.0e-6 * segment_len;
    if (alpha_l < epsilon || alpha_r < epsilon) {
		alpha_l = alpha_r = segment_len/3;
		printf("bad\n");
	}

	bezier[1] = add(start_point, scale(l_tangent,  alpha_l));
	bezier[2] = add(end_point,  scale(r_tangent, alpha_r));
    bezier[3] = end_point;
}

void optimize(Point* points, int points_count, Point *bezier, int segments)
{
	int start, end, segment_len = (points_count-1) / segments, prev, next, i, j;
	Point l_tangent, r_tangent;

	double *offsets = calloc(segment_len, sizeof(double));

	bezier[0] = points[0];
	for (i = 0; i < segments; i++) {
		start = i * segment_len;
		end = start + segment_len;

		prev = start == 0 ? 0 : start - 1;
		next = start + 1;
		l_tangent = direction(points[prev], points[next]);

		prev = end - 1;
		next = end == points_count-1 ? end : end + 1;
		r_tangent = direction(points[next], points[prev]);

		offsets[0] = 0;
		for (j = 1; j < segment_len; j++) {
			offsets[j] = offsets[j-1] + distance(points[start+j-1], points[start+j]);
		}
		for (j = 1; j < segment_len; j++) {
			offsets[j] = offsets[j] / offsets[segment_len-1];
		}

		generateBezier(&points[start], segment_len, l_tangent, r_tangent, offsets, &bezier[i*3]);

		reparameterize(&points[start], segment_len, offsets, &bezier[i*3]);
		generateBezier(&points[start], segment_len, l_tangent, r_tangent, offsets, &bezier[i*3]);
		reparameterize(&points[start], segment_len, offsets, &bezier[i*3]);
		generateBezier(&points[start], segment_len, l_tangent, r_tangent, offsets, &bezier[i*3]);
		reparameterize(&points[start], segment_len, offsets, &bezier[i*3]);
		generateBezier(&points[start], segment_len, l_tangent, r_tangent, offsets, &bezier[i*3]);
	}
	free(offsets);
}


int equirectangular(double ix, double iy, double *x, double *y, double f0)
{
	*x = ix;
	*y = iy;
	return 1;
}

void equirectangular_g(double w, double h, double xo, double yo, int step)
{
	MERIDIANS
	for (double i = 0; i < 360; i+= step) {
		double x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;

		line(i-180, x, 0, x, h);
	}
	PARALLELS
	for (double i = step; i < 180; i+= step) {
		double y = i/180 * h;
		line(90-i, 0, y, w, y);
	}
	outline_frame(w, h);
}

int sphere3d(double ix, double iy, double *x, double *y, double f0)
{
	double xx = ix * 2 - 1;
	double yy = iy * 2 - 1;
	double p = xx*xx + yy*yy;
	if (p >= 1) return 0;

	double cosc = sqrt(1-p);
	p = sqrt(p);
	double sinf = sin(f0);
	double cosf = cos(f0);

	*y = asin(cosc*sinf + (yy*p * cosf) / p);
	*x = atan2((xx * p) , (p*cosc*cosf - yy*p*sinf));

	*x = (*x / M_PI / 2) + 0.5f;
	*y = (*y / M_PI ) + 0.5f;

	return 1;
}
void sphere3d_g(double w, double h, double xo, double yo, int step)
{
	outline_round_frame(w, h);
}

int lambert_cyl(double ix, double iy, double *x, double *y, double f0)
{
	*x = ix;
	*y = asin(iy * 2 - 1) / M_PI + 0.5;
	return 1;
}
void lambert_cyl_g(double w, double h, double xo, double yo, int step)
{
	MERIDIANS
	for (double i = 0; i < 360; i+= step) {
		double x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;

		line(i-180, x, 0, x, h);
	}
	PARALLELS
	for (double i = -90+step; i < 90; i+= step) {
		double y = (1-sin(i/180*M_PI))/2 * h;
		line(i, 0, y, w, y);
	}
	outline_frame(w, h);
}

int mercator(double ix, double iy, double *x, double *y, double f0)
{
	*x = ix;

	double yy = (iy - 0.5) * 1.5 * M_PI;
	double range = atan(exp(M_PI)) - M_PI / 4;

	*y = atan(exp(yy))- M_PI / 4;
	*y /= range;
	*y = *y / 2 + 0.5;

	return 1;
}
void mercator_g(double w, double h, double xo, double yo, int step)
{
	outline_frame(w, h);
}

int cassini(double ix, double iy, double *x, double *y, double f0)
{
	double range = 2 * (M_PI - asin(1));

	double xx = (ix - 0.5) * range;
	double yy = (iy - 0.5) * range * 2;

	*x = atan2(tan(xx), cos(yy));
	*y = asin(sin(yy)*cos(xx));

	*x = (*x / (M_PI * 2)) + 0.5;
	*y = (*y / range) + 0.5;
	return 1;
}

void cassini_g(double w, double h, double xo, double yo, int step)
{
	outline_frame(w, h);
}

int hammer(double ix, double iy, double *x, double *y, double f0)
{
	double xx = ix * 2 - 1;
	double yy = iy * 2 - 1;
	double distSqr = xx*xx + yy*yy;

	if (distSqr >= 1) return 0;

	xx *= sqrt(2) * 2;
	yy *= sqrt(2);

	double z = sqrt(1 - (xx*xx)/16 - (yy*yy)/4 );

	*x = atan((z*xx) / (2 * (2*z*z - 1)));
	*y = asin(z * yy);


	*x = (*x / M_PI) + 0.5f;
	*y = (*y / M_PI) + 0.5f;

	return 1;
}

void hammer_f(double l, double f, double w, double h, Point *o)
{
	double p = sqrt(1+cos(f)*cos(l/2));
	o->x = p == 0 ? w/2 : (cos(f)*sin(l/2) / p) * w/2 + w/2;
	o->y = p == 0 ? (f > 0 ? 0 : h) : -(sin(f) / p) * h/2 + h/2;
}
void hammer_g(double w, double h, double xo, double yo, int step)
{
	int i = 0;
	double l, f, microstep = M_PI / MICROSTEP_FACTOR;

#define SEGMENTS 12

	Point *points = calloc(MICROSTEP_FACTOR*2+1, sizeof(Point));
	Point besier[SEGMENTS*3+1];

	MERIDIANS;
	for	(double ld = 0; ld < 360; ld += step) {
		l = ld/360-xo;
		l -= (int)l;
		if (l < 0) l +=1;
		l = (l - 0.5) * M_PI * 2;

		for (i = 0; i < MICROSTEP_FACTOR+1; i++) {
			hammer_f(l, f = -M_PI/2 + i * microstep, w, h, &points[i]);
		}
		optimize(points, MICROSTEP_FACTOR+1, (Point*)(&besier), SEGMENTS);

		path(ld-180, besier[0].x, besier[0].y);
		set_smooth(1);
		for (i = 1; i < SEGMENTS*3+1; i++) {
			add_point(besier[i].x, besier[i].y);
		}
		set_smooth(0);
		end_path(0);
	}

	PARALLELS
	for	(double fd = -90+step; fd < 90; fd += step) {
		f = fd/180 * M_PI;

		for (i = 0; i < MICROSTEP_FACTOR*2+1; i++) {
			hammer_f(-M_PI + i * microstep, f, w, h, &points[i]);
		}

		optimize(points, MICROSTEP_FACTOR*2+1, (Point*)(&besier), SEGMENTS);

		path(fd, besier[0].x, besier[0].y);
		set_smooth(1);
		for (i = 1; i < SEGMENTS*3+1; i++) {
			add_point(besier[i].x, besier[i].y);
		}
		set_smooth(0);
		end_path(0);
	}

	free(points);
	outline_round_frame(w, h);
#undef SEGMENTS
}

int mollweide(double ix, double iy, double *x, double *y, double f0)
{
	double xx = ix - 0.5;
	double yy = iy - 0.5;
	double dist = sqrt(xx*xx + yy*yy);

	if (dist >= 0.5) return 0;

	xx *= 2 * sqrt(2);
	yy *= 2 * sqrt(2);

	double t = asin(yy / sqrt(2));
	double t2 = t*2;

	*x = (M_PI * xx) / (2 * sqrt(2) * cos(t));
	*y = asin((t2 + sin(t2)) / M_PI);

	*x = (*x / M_PI) + 0.5;
	*y = (*y / M_PI) + 0.5;
	return 1;
}

void mollweide_g(double w, double h, double xo, double yo, int step)
{
	double i, y, x, t, size;
	MERIDIANS
	for (i = 0; i < 360; i+= step) {
		x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;
		arc(i-180, w/2, 0, w/2, h, x-w/2, x > w/2);
	}

	PARALLELS
	for (i = -90+step; i < 90; i+= step) {
		y = i/180*M_PI;
		t = y;
		for (int k = 0; k < 6; k++) {
			t -= (2*t+sin(2*t)-M_PI*sin(y)) / (2+2*cos(2*t));
		}
		y = (1-sin(t))/2 * h;
		size = (cos(t))/2 * w;
		line(i, w/2-size, y, w/2+size, y);
	}

	outline_round_frame(w, h);
}

int wagner6(double ix, double iy, double *x, double *y, double f0)
{
	*y = iy;
	*x = (ix-0.5) / cos(asin(sqrt(3)*(iy-0.5))) + 0.5;
	return *x >= 0 && *x <= 1;
}
void wagner6_g(double w, double h, double xo, double yo, int step)
{
}

int azimut(double ix, double iy, double *x, double *y, double f0)
{
	double xx = ix - 0.5;
	double yy = iy - 0.5;
	double distSqr = xx*xx + yy*yy;
	if (distSqr >= 0.25) return 0;
	*x = (atan2(xx, yy) + M_PI) / (M_PI*2);
	*y = sqrt(distSqr)*2;
	return 1;
}

void azimut_g(double w, double h, double xo, double yo, int step)
{
	outline_round_frame(w, h);
}

int sinusoidal(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5)*2;
	double yy = (iy - 0.5) * M_PI;

	*x = xx / cos(yy) / 2 + 0.5;

	if (*x > 1 || *x < 0) return 0;

	*y = iy;
	return 1;
}
void sinusoidal_g(double w, double h, double xo, double yo, int step)
{
	MERIDIANS
	for (double i = 0; i < 360; i+= step) {
		double x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;

		path(i-180, w/2, 0);
		set_smooth(1);
		add_sinevawe(x-w/2, h);
		set_smooth(0);
		end_path(0);
	}

	PARALLELS
	for (double i = -90+step; i < 90; i+= step) {
		double y = (1 - i/90) * h/2;
		double x = cos(i/180 * M_PI) * w/2;
		line(i, w/2-x, y, w/2+x, y);
	}

	outline_path(w/2, 0);
	set_smooth(1);
	add_sinevawe(-w/2, h);
	add_sinevawe(w/2, -h);
	set_smooth(0);
	end_path(1);

}

int eckert2(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * 2;
	double yy = (iy - 0.5) * 2;

	*x = xx / (4 - 2*fabs(yy));
	*x = (*x * 2) + 0.5;

	if (*x > 1 || *x < 0) return 0;

	double part = (2 - (fabs(yy)));
	*y = fabs((asin((4 - part*part) / 3)));
	if (yy < 0) *y *= -1;
	*y = (*y / M_PI) + 0.5;
	if (*y > 1 || *y < 0) return 0;

	return 1;
}
void eckert2_g(double w, double h, double xo, double yo, int step)
{
	MERIDIANS
	for (double i = 0; i < 360; i+= step) {
		double x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;
		path(i-180, w/4+x/2, 0);
		add_point(x, h/2);
		add_point(w/4+x/2, h);
		end_path(0);
	}
	PARALLELS
	for (double i = -90+step; i < 90; i+= step) {
		double y = fabs(2-sqrt(4-3*sin(fabs(i/180*M_PI))));
		double margin = w*0.25*y;
		if (i < 0) y = -y;
		y = (y+1)/2*h;

		line(i, margin, y, w-margin, y);
	}

	outline_polygon(6, 0.,h/2, w*0.25,h, w*0.75,h, w,h/2, w*0.75,0., w*0.25,0.);
}

int eckert4(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * 2;
	double yy = (iy - 0.5) * 2;

	double t = asin(yy);
	double cost = cos(t);

	*x = xx / ((1 + cost)) + 0.5;
	if (*x > 1 || *x < 0) return 0;

	double sint = sin(t);
	*y = asin((t + sint*cost + 2*sint) / (2 + M_PI*0.5));
	*y = (*y / M_PI) + 0.5;

	if (*y > 1 || *y < 0) return 0;

	return 1;
}
void eckert4_g(double w, double h, double xo, double yo, int step)
{
	double i, y, x, t, size;
	MERIDIANS
	for (i = 0; i < 360; i+= step) {
		x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;
		arc(i-180, w/4+x/2, 0, w/4+x/2, h, x/2-w/4, x > w/2);
	}

	PARALLELS
	for (i = -90+step; i < 90; i+= step) {
		y = i/180*M_PI;
		t = y/2;
		for (int k = 0; k < 6; k++) {
			t -= (t + sin(t)*cos(t) + 2*sin(t) - (2+M_PI/2)*sin(y)) / ( 2*cos(t)*(1+cos(t)));
		}

		y = (-sin(t)+1)/2*h;
		size = (1+cos(t))/4 * w;
		line(i, w/2-size, y, w/2+size, y);
	}

	outline_path(w*0.25, 0);
	add_point(w*0.75, 0);
	add_arc(w*0.75, h, w/4, 1);
	add_point(w*0.25, h);
	add_arc(w*0.25, 0, w/4, 1);
	end_path(1);
}

int eckert6(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * M_PI*2;
	double yy = (iy - 0.5) * M_PI;

	*x = xx / (1+cos(yy));
	*x = (*x / M_PI) + 0.5;
	if (*x > 1 || *x < 0) return 0;

	*y = asin((yy + sin(yy)) / (1 + M_PI*0.5));
	*y = (*y / M_PI) + 0.5;
	if (*y > 1 || *y < 0) return 0;

	return 1;
}
void eckert6_g(double w, double h, double xo, double yo, int step)
{
	double i, y, x, dt, t, size;

	MERIDIANS
	for (i = 0; i < 360; i+= step) {
		x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		x *= w;

		path(i-180, w/4+x/2, 0);
		set_smooth(1);
		add_sinevawe((x-w/2)/2, h);
		set_smooth(0);
		end_path(0);
	}

	PARALLELS
	for (i = -90+step; i < 90; i+= step) {
		y = i/180*M_PI;
		t = y;
		for (int k = 0; k < 6; k++) {
			dt = (t + sin(t)  - (1+M_PI/2)*sin(y)) / (1+cos(t));
			t-= dt;
		}

		y = h/2 - t / M_PI * h;
		size = (1+cos(t))/4 * w;
		line(i, w/2-size, y, w/2+size, y);
	}

	outline_path(w*0.25, 0);
	add_point(w*0.75, 0);
	set_smooth(1);
	add_sinevawe(w/4, h);
	set_smooth(0);
	add_point(w*0.25, h);
	set_smooth(1);
	add_sinevawe(-w/4, -h);
	set_smooth(0);
	end_path(1);

}

int collingnon(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * M_PI;

	*y = -asin(1 - iy*iy*2);
	*x = xx / sin(M_PI/4 + *y/2);

	*x = (*x / M_PI) + 0.5;
	*y = (*y / M_PI) + 0.5;

	if (*x > 1 || *x < 0) return 0;
	if (*y > 1 || *y < 0) return 0;

	return 1;
}
void collingnon_g(double w, double h, double xo, double yo, int step)
{
	MERIDIANS
	for (double i = 0; i < 360; i+= step) {
		double x = i/360 - xo;
		x -= (int)x;
		if (x < 0) x +=1;
		line(i-180, w/2, 0, x*w, h);
	}

	PARALLELS
	for (double i = -90+step; i < 90; i+= step) {
		double y = (1-sqrt(1-sin(i/180*M_PI)));
		y = 1+(1-y-sqrt(2))/sqrt(2);
		line(i, w*(1-y)/2, y*h, w*(1+y)/2, y*h);
	}

	outline_polygon(3, 0.0,h, w/2,0.0, w,h);
}

#define WERNER_B -M_PI
#define WERNER_U 0.917626
#define WERNER_WIDTH 4.043221
#define WERNER_ASPECT WERNER_WIDTH/(WERNER_U-WERNER_B)

int werner(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * WERNER_WIDTH;
	double yy = (1-iy) * (WERNER_U-WERNER_B) + WERNER_B ;

	double p = sqrt(xx*xx + yy*yy);

	*y = p - M_PI/2;
	*x = p / cos(*y) * atan2(xx, -yy);

	*y = (*y / M_PI) + 0.5;
	*x = (*x / M_PI / 2) + 0.5;

	if (*x > 1 || *x < 0) return 0;
	if (*y > 1 || *y < 0) return 0;

	return 1;
}

void werner_f(double ix, double iy, double w, double h, Point *o)
{
	double p = M_PI/2 - iy;
	double E = p == 0 ? 0 : ix * cos(iy)/p;
	o->x = w/2 + p*sin(E) / WERNER_WIDTH * w;
	o->y = -(-p*cos(E) - WERNER_U) / (WERNER_U-WERNER_B)*h;
}

void werner_g(double w, double h, double xo, double yo, int step)
{
	int i = 0;
	double l, f, microstep = M_PI / MICROSTEP_FACTOR;

#define SEGMENTS 24

	Point *points = calloc(MICROSTEP_FACTOR*2+1, sizeof(Point));
	Point besier[SEGMENTS*3+1];

	MERIDIANS;
	for	(double ld = 0; ld < 360; ld += step) {
		l = ld/360-xo;
		l -= (int)l;
		if (l < 0) l +=1;
		l = (l - 0.5) * M_PI * 2;

		for (i = 0; i < MICROSTEP_FACTOR+1; i++) {
			werner_f(l, -M_PI/2 + i * microstep, w, h, &points[i]);
		}
		optimize(points, MICROSTEP_FACTOR+1, (Point*)(&besier), SEGMENTS);

		path(ld-180, besier[0].x, besier[0].y);
		set_smooth(1);
		for (i = 1; i < SEGMENTS*3+1; i++) {
			add_point(besier[i].x, besier[i].y);
		}
		set_smooth(0);
		end_path(0);
	}

	PARALLELS
	for	(double fd = -90+step; fd < 90; fd += step) {
		f = fd/180 * M_PI;

		for (i = 0; i < MICROSTEP_FACTOR*2+1; i++) {
			werner_f(-M_PI + i * microstep, f, w, h, &points[i]);
		}
		optimize(points, MICROSTEP_FACTOR*2+1, (Point*)(&besier), SEGMENTS);

		path(fd, besier[0].x, besier[0].y);
		set_smooth(1);
		for (i = 1; i < SEGMENTS*3+1; i++) {
			add_point(besier[i].x, besier[i].y);
		}
		set_smooth(0);
		end_path(0);
	}

	for (i = 0; i < MICROSTEP_FACTOR+1; i++) {
		werner_f(-M_PI, -M_PI/2 + i * microstep, w, h, &points[i]);
	}
	optimize(points, MICROSTEP_FACTOR+1, (Point*)(&besier), SEGMENTS);

	outline_path(besier[0].x, besier[0].y);
	set_smooth(1);

	for (i = 1; i < SEGMENTS*3+1; i++) {
		add_point(besier[i].x, besier[i].y);
	}

	for (i = 0; i < MICROSTEP_FACTOR+1; i++) {
		werner_f(M_PI, +M_PI/2 - i * microstep, w, h, &points[i]);
	}
	optimize(points, MICROSTEP_FACTOR+1, (Point*)(&besier), SEGMENTS);
	for (i = 1; i < SEGMENTS*3+1; i++) {
		add_point(besier[i].x, besier[i].y);
	}

	set_smooth(0);
	end_path(1);

	free(points);
#undef SEGMENTS
}

int vandergrinten(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * 2;
	double yy = (iy - 0.5) * 2;

	double X2 = xx*xx;
	double Y2 = yy*yy;
	double XY = X2 + Y2;

	if (XY > 1) return 0;

	double c1 = -fabs(yy)*(1 + XY);
	double c2 = c1 - 2*Y2 + X2;
	double c3 = -2*c1 + 1 + 2*Y2 + XY*XY;
	double d = Y2/c3 + (2*pow(c2, 3)/pow(c3, 3) - 9*c1*c2/pow(c3, 2)) / 27;
	double a1=(c1 - c2*c2/(3*c3)) / c3;
	double m1 = 2*sqrt(-a1/3);
	double t1=acos(3*d/(a1*m1))/3;

	*y = (-m1 * cos(t1+M_PI/3)-c2/(3*c3));

	if (yy < 0)
		*y = -*y;
	else if (yy == 0)
		*y = 0;

	*x = (XY - 1 + sqrt(1 + 2*(X2-Y2) + XY*XY)) / (2*xx);
	if(xx == 0) *x = 0;

	*y += 0.5;
	*x = (*x / 2) + 0.5;

	return 1;
}
void vandergrinten_g(double w, double h, double xo, double yo, int step)
{
	outline_round_frame(w, h);
}


int american_polyconic(double ix, double iy, double *x, double *y, double f0)
{
	double xx = (ix - 0.5) * 2;
	double yy = (iy - 0.5) * M_PI;

	if (yy == 0) {
		*y = 0.5;
		*x = (xx / M_PI*2) + 0.5 ;
		return *x < 1 && *x >= 0;
	}

	double A = latitude_offset + yy;
	double B = xx*xx + A*A;

	double f = A/2;
	double tanf, df, secf, j;

	for (int i=0; i < 20; i++) {
		tanf = tan(f);
		secf = 1 / cos(f);
		j = B - 2*yy*f + f*f;
		//df = (A*(f*tanf+1) - f - 0.5*(f*f+B)*tanf) / ((f-A)/tanf - 1);
		df = (tanf * j + 2 *(f-yy)) / (2+j*secf*secf + 2 * (f-yy) * tanf);
		f-=df;
		if(fabs(df) < 0.0001)
			break;
	}
	tanf = tan(f);

	*y = (f / M_PI) + 0.5;
	if (*y > 1 || *y < 0) return 0;

	//*x = asin(xx*tan(f)) / sin(f);
	double signx = xx > 0 ? 1 : -1;
	double signy = yy > 0 ? 1 : -1;

	double alt = abs(yy) < abs(f + 1 / tanf)
		? asin(xx * tanf)
		: signy * signx * (acos(abs(xx * tanf)) + M_PI/2);

	alt = asin(xx * tanf);

	*x = alt / sin(f);
	*x = (*x / M_PI*2) + 0.5;

	if (*x > 1 || *x < 0) return 0;

	return 1;
}

void american_polyconic_g(double w, double h, double xo, double yo, int step)
{
	outline_round_frame(w, h);
}
