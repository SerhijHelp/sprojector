#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/inotify.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <regex.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <time.h>
#include <pthread.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>
#include <X11/XF86keysym.h>
#include <sys/poll.h>

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <libnotify/notify.h>

#include <X11/Xft/Xft.h>
#include <math.h>
#include <png.h>
#include <jpeglib.h>

#include "arg.h"
#include "util.h"
#include "imsvg.h"

char *argv0;

#define INOTIFY_BUFFER_SIZE 8192
#define PI_HALF (M_PI/2)
#define CLEANMASK(mask)         (mask & ~(LockMask) & (ShiftMask|ControlMask))
#define SPHERE3D_SELECTED       (projectionId == sphere3d_id)
#define LEN(a)         (sizeof(a) / sizeof(a)[0])
#define rside 0
#define wside 1
#define isnum(a) (a >= '0' && a <= '9')

enum { WmDeleteWin, XdndEnter, XdndPosition, XdndDrop, XdndLeave, ProtocolsCount };

typedef enum { ForScreen, ExportPng, ExportJpg } UvMode;

typedef union {
	int i;
	unsigned int ui;
	float f;
	const void *v;
} Arg;

typedef struct {
	char *regex;
	char *bin;
} Filter;

typedef struct {
	unsigned char *buf;
	unsigned int width, height;
} Image;

typedef struct {
	char mask;
	int x, y;
} UvCell;

typedef struct {
	UvCell *buf;
	unsigned int width, height;
} UvMap;

typedef struct {
	unsigned int mod;
	KeySym keysym;
	void (*func)(const Arg *);
	const Arg arg;
} Key;

typedef struct {
	char *name;
	double aspect;
	int (*func)(double, double, double*, double*, double);
	void (*svg)(double, double, double, double, int);
} Projection;


static void *new_uv_p(void *arg);
static void *project_p(void *arg);
static void close_window();
static void createimg();
static void draw();
static void enablegrid();
static void exportimg();
static void flip();
static void jpeg_error(j_common_ptr js);
static void keypress(XKeyEvent*);
static void loadjpg(FILE* file);
static void loadorig();
static void loadpng(FILE* file);
static void new_uv(int w, int h, int update_size);
static void offset_latitude(const Arg *arg);
static void offset_longitude(const Arg *arg);
static void png_err(png_struct *pngs, const char *msg);
static void prepare_exportable(int w, int h);
static void project();
static void projectionmenu();
static void run();
static void updatetitle();
static void writejpg(FILE *file, Image *img);
static void writepng(FILE *file, Image *img);
static void writesvg();


double longitude_offset = 0, latitude_offset = 0;

#if UKR == 1
#include "ukrainian.h"
#else
#include "english.h"
#endif

#include "projections.h"
#include "urldecode.h"
#include "config.h"

static char *opt_embed = NULL;
static char *filename = NULL, *dropped_filename = NULL, export_filename[PATH_MAX] = "projection.jpg";
static Window root, win, sender;
static Atom mime_type, XdndSelection, XdndStatus, XdndActionCopy, XdndTypeList, XdndFinished, protocols[ProtocolsCount];
Display *display;
int screenId;
int winW = 800, winH = 400, mousex, drag = 0;
int projectionId = 0;
double mark_lon;
int new_file_is_valid = 0, status_sent = 0, lines_enabled = 0, monitor, orig_loaded = 0, grid_enabled = 0, stop_threads=0, flipped=0, export_mode = 0;
struct pollfd fds[1];
pthread_barrier_t uv_in_barrier, uv_out_barrier, screen_in_barrier, screen_out_barrier;
Time last_mouse_drag;
XImage *img;
Image orig = {0}, exportable = {0};
UvMap uv = {0};
UvMode uv_mode = ForScreen;
GC gc;


void
writesvg() {
	FILE *file = fopen("projection.png", "w");
	int w, h;
	w = 6000;
	h= 3000;

	// new_uv(&export_map, w, h, 1);
	writepng(file, &exportable);
	fclose(file);

	start_svg("projection.svg", "projection.png", w, h);
	projections[projectionId].svg(w, h, longitude_offset, latitude_offset, GRID_STEP);
	end_svg();
}

void
writepng(FILE *file, Image *img) {
	png_struct *pngs;
	png_info *pngi;
	int y;

	/* prepare */
	pngs = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, png_err, NULL);
	pngi = png_create_info_struct(pngs);

	if (!pngs || !pngi) {
		die("Failed to initialize libpng");
	}

	png_init_io(pngs, file);
	png_set_IHDR(pngs, pngi, img->width, img->height, 16, PNG_COLOR_TYPE_RGB_ALPHA,
	             PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_write_info(pngs, pngi);

	//row = ereallocarray(NULL, img->width, (sizeof("RGBA") - 1) * sizeof(uint16_t));

	for (y = 0; y < img->height; y++) {
		png_write_row(pngs, (uint8_t*)(img->buf[y*img->width*4]));
	}

	/* clean up */
	png_write_end(pngs, NULL);
	png_destroy_write_struct(&pngs, NULL);
}

void
writejpg(FILE *file, Image *img) {
	struct jpeg_compress_struct jcomp;
	struct jpeg_error_mgr jerr;
	int y;

	/* prepare */
	jpeg_create_compress(&jcomp);
	jerr.error_exit = jpeg_error;
	jcomp.err = jpeg_std_error(&jerr);

	jpeg_stdio_dest(&jcomp, file);
	jcomp.image_width = exportable.width;
	jcomp.image_height = exportable.height;
	jcomp.input_components = 3;     /* color components per pixel */
	jcomp.in_color_space = JCS_RGB; /* output color space */
	jpeg_set_defaults(&jcomp);

	//jcomp.optimize_coding = 1;
	jpeg_set_quality(&jcomp, 85, 1);
	jpeg_start_compress(&jcomp, 1);

	JSAMPROW rowout [1];

	/* write data */
	for (y = 0; y < exportable.height; y++) {
		rowout[0] = &(exportable.buf[y*exportable.width*3]);
		jpeg_write_scanlines(&jcomp, rowout, 1);
	}

	/* clean up */
	jpeg_finish_compress(&jcomp);
	jpeg_destroy_compress(&jcomp);
}


static void
png_err(png_struct *pngs, const char *msg) {
	(void)pngs;
	die("libpng: %s", msg);
}

void
loadpng(FILE *file) {
	png_struct *pngs;
	png_info *pngi;
	uint32_t width, height, r, i;
	uint8_t **pngrows;
	int offset = 0;

	/* prepare */
	pngs = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, png_err, NULL);
	pngi = png_create_info_struct(pngs);

	if (!pngs || !pngi) {
		die("Failed to initialize libpng");
	}

	png_init_io(pngs, file);
	if (png_get_valid(pngs, pngi, PNG_INFO_tRNS)) {
		png_set_tRNS_to_alpha(pngs);
	}
	png_set_add_alpha(pngs, 255*257, PNG_FILLER_AFTER);
	png_set_expand_gray_1_2_4_to_8(pngs);
	png_set_gray_to_rgb(pngs);
	png_set_packing(pngs);
	png_read_png(pngs, pngi, PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

	pngrows = png_get_rows(pngs, pngi);

	width = png_get_image_width(pngs, pngi);
	height = png_get_image_height(pngs, pngi);

	free(orig.buf);
	orig.width = width;
	orig.height = height;
	orig.buf = ecalloc(width * height, sizeof("RGB"));


	switch(png_get_bit_depth(pngs, pngi)) {
	case 8:
		for (r = 0; r < height; ++r) {
			for (i = 0; i < width; i++) {
				orig.buf[offset++] = htons(257 * pngrows[r][i*4+0]);
				orig.buf[offset++] = htons(257 * pngrows[r][i*4+1]);
				orig.buf[offset++] = htons(257 * pngrows[r][i*4+2]);
			}
		}
		break;
	case 16:
		for (r = 0; r < height; ++r) {
			for (i = 0; i < width; i++) {
				orig.buf[offset++] = pngrows[r][2*(i*4+0)];
				orig.buf[offset++] = pngrows[r][2*(i*4+1)];
				orig.buf[offset++] = pngrows[r][2*(i*4+2)];
			}
		}
		break;
	default:
		die("Invalid bit-depth");
	}

	/* clean up */
	png_destroy_read_struct(&pngs, &pngi, NULL);

}

static void
jpeg_error(j_common_ptr js) {
	fprintf(stderr, "%s: libjpeg: ", argv0);
	(*js->err->output_message)(js);
	exit(1);
}

void
loadjpg(FILE *file) {
	struct jpeg_decompress_struct js;
	struct jpeg_error_mgr jerr;
	uint32_t width, height;
	uint8_t *rowin;
	int i, offset = 0;

	/* prepare */
	jpeg_create_decompress(&js);
	jerr.error_exit = jpeg_error;
	js.err = jpeg_std_error(&jerr);

	jpeg_stdio_src(&js, file);
	jpeg_read_header(&js, 1);
	js.output_components = 3;     /* color components per pixel */
	js.out_color_space = JCS_RGB; /* input color space */

	jpeg_start_decompress(&js);

	width = js.image_width;
	height = js.image_height;

	rowin = ereallocarray(NULL, width, (sizeof("RGB") - 1) * sizeof(uint8_t));

	free(orig.buf);
	orig.width = width;
	orig.height = height;
	orig.buf = ecalloc(width * height, sizeof("RGB"));

	while (js.output_scanline < js.output_height) {
		jpeg_read_scanlines(&js, &rowin, 1);
		for (i = 0; i < width; ++i) {
			orig.buf[offset++] = htons(rowin[i * 3 + 0] * 257);
			orig.buf[offset++] = htons(rowin[i * 3 + 1] * 257);
			orig.buf[offset++] = htons(rowin[i * 3 + 2] * 257);
		}
	}

	/* clean up */
	jpeg_finish_decompress(&js);
	jpeg_destroy_decompress(&js);
	free(rowin);
}


void
loadorig(int from_dropped) {
	char *name;
	unsigned char buffer[8];
	FILE *file;
	name = from_dropped ? dropped_filename : filename;
	if (name == NULL) return;
	if ((file = fopen(name, "r")) == NULL) {
		printf("Unable to open %s\n", name);
		return;
	}

	fread(buffer, 8, 1, file);
	rewind(file);

	if (buffer[0]  == 0xff &&
		buffer[1]  == 0xd8 &&
		buffer[2]  == 0xff) {
		loadjpg(file);
	} else if (buffer[0] == 0x89 &&
		buffer[1] == 'P'  &&
		buffer[2] == 'N'  &&
		buffer[3] == 'G'  &&
		buffer[4] == 0x0d  &&
		buffer[5] == 0x0a  &&
		buffer[6] == 0x1a  &&
		buffer[7] == 0x0a) {
		loadpng(file);
	} else {
		printf("Unable to understand the content of %s\n", name);
		return;
	}

	if (from_dropped) {
		inotify_rm_watch (fds[0].fd, monitor);
		close (fds[0].fd);
		filename = dropped_filename;
		fds[0].fd = inotify_init();
		monitor = inotify_add_watch(fds[0].fd, filename, IN_CLOSE_WRITE);
	}

	fclose(file);
	orig_loaded = 1;
	createimg();
}

void
createimg() {
	double aspect, w, h;

	if (!orig_loaded) return;
	aspect = projections[projectionId].aspect;
	if (aspect < 0) {
		w = winW;
		h = winH;
	} else {
		if (winH * aspect > winW) {
			h = winW / aspect;
			w = winW;
		} else {
			h = winH;
			w = winH * aspect;
		}
	}

	if(img)
		XDestroyImage(img);
	int depth = DefaultDepth(display, screenId);
	if (!(img = XCreateImage(display, CopyFromParent, depth, ZPixmap, 0, NULL, w, h, 32, 0)))
		die("sent: Unable to create XImage");

	img->data = ecalloc(img->height, img->bytes_per_line);
	if (!XInitImage(img))
		die("sent: Unable to initiate XImage");

	new_uv(img->width, img->height, 1);
	project();
}

void*
new_uv_p(void *arg) {
	int w, h, x, y, from, to, offset = 0;
	int64_t p;
	char r, g, b;
	size_t chunk = *(size_t*)arg;
	double xo, yo;

	while (1) {
		pthread_barrier_wait(&uv_in_barrier);
		if(stop_threads) return NULL;

		if (export_mode != ForScreen) {
			w = exportable.width;
			h = exportable.height;
			from = h / UV_THREADS * chunk;
			to = chunk < UV_THREADS-1 ? h / UV_THREADS * (chunk+1) : h;

			for (y = from; y < to; y++) {
				for (x = 0; x < w;  x++) {
					offset = y*w + x;
					if(projections[projectionId].func((double)x / w, (double)y / h, &xo, &yo, latitude_offset)) {
						xo += longitude_offset;
						xo -= (int)xo;
						if (xo < 0) xo += 1;

						xo *= orig.width;
						yo *= orig.height;
						xo = (int64_t)xo;
						yo = (int64_t)yo;
						p = (yo * orig.width + xo) * 3;

						if (export_mode == ExportPng) {
							exportable.buf[4*offset+0] = orig.buf[p+0];
							exportable.buf[4*offset+1] = orig.buf[p+1];
							exportable.buf[4*offset+2] = orig.buf[p+2];
							exportable.buf[4*offset+3] = 0xFF;
						} else {
							exportable.buf[3*offset+0] = ntohs(orig.buf[p+0] << 8);
							exportable.buf[3*offset+1] = ntohs(orig.buf[p+1] << 8);
							exportable.buf[3*offset+2] = ntohs(orig.buf[p+2] << 8);
						}
					} else {
						if (export_mode == ExportPng) {
							exportable.buf[4*offset+0] = (unsigned short)(png_fill << 24);
							exportable.buf[4*offset+1] = (unsigned short)(png_fill << 16);
							exportable.buf[4*offset+2] = (unsigned short)(png_fill << 8);
							exportable.buf[4*offset+3] = (unsigned short)(png_fill << 0);
						} else {
							exportable.buf[3*offset+0] = ntohs((unsigned short)(jpg_fill >> 8));
							exportable.buf[3*offset+1] = ntohs((unsigned short)jpg_fill);
							exportable.buf[3*offset+2] = ntohs((unsigned short)(jpg_fill << 8));
						}
					}
				}
			}
		} else {
			w = uv.width;
			h = uv.height;
			from = h / UV_THREADS * chunk;
			to = chunk < UV_THREADS-1 ? h / UV_THREADS * (chunk+1) : h;

			for (y = from; y < to; y++) {
				for (x = 0; x < w;  x++) {
					offset = y*w + x;
					int m = projections[projectionId].func((double)x / w, (double)y / h, &xo, &yo, latitude_offset);
					if(m) {
						uv.buf[offset].mask = 1;
						uv.buf[offset].x = (int64_t)(xo * orig.width);
						uv.buf[offset].y = (int64_t)(yo * orig.height);
					} else {
						uv.buf[offset].mask = 0;
					}
				}
			}
		}
		pthread_barrier_wait(&uv_out_barrier);
	}
}

void
new_uv(int w, int h, int update_size) {
	if(!orig_loaded) return;

	if (update_size) {
		uv.width = w;
		uv.height = h;
		free(uv.buf);
		uv.buf = ecalloc(w * h, sizeof(UvCell));
	}

    pthread_barrier_wait(&uv_in_barrier);
    pthread_barrier_wait(&uv_out_barrier);
}

void
prepare_exportable(int w, int h) {
	if(!orig_loaded) return;

	exportable.width = w;
	exportable.height = h;
	free(exportable.buf);
	exportable.buf = ecalloc(w * h, uv_mode == ExportPng ? sizeof(uint16_t)*4 : sizeof(uint8_t)*3);

    pthread_barrier_wait(&uv_in_barrier);
    pthread_barrier_wait(&uv_out_barrier);
}

void*
project_p(void *arg) {
	int64_t x, y, xo, yo, p, offset, img_offset, from, to;
	size_t chunk = *(size_t*)arg;

	while (1) {
		pthread_barrier_wait(&screen_in_barrier);
		if(stop_threads) return NULL;

		from = img->height / SCREEN_THREADS * chunk;
		to = chunk < SCREEN_THREADS-1 ? img->height / SCREEN_THREADS * (chunk+1) : img->height;

		for (y = from; y < to; y++) {
			for (x = 0; x < img->width; x++) {
				offset = y*img->width + x;
				img_offset = offset * 4;

				xo = uv.buf[offset].x;
				yo = uv.buf[offset].y;
				if (flipped) yo = orig.height - yo - 1;

				if (uv.buf[offset].mask) {
					xo = (xo + (int64_t)(longitude_offset * orig.width)) % (int64_t)orig.width;
					if (xo < 0) xo += orig.width;

					if (grid_enabled && (yo % (orig.height/6) < (orig.width/1000.0) || xo % (orig.width/12) < (orig.width/1000.0))) {
						img->data[img_offset+2] = (char)(grid_color >> 16);
						img->data[img_offset+1] = (char)(grid_color >> 8);
						img->data[img_offset+0] = (char)(grid_color);
					} else {
						p = (yo * orig.width + xo) * 3;
						img->data[img_offset+2] = orig.buf[p+0];
						img->data[img_offset+1] = orig.buf[p+1];
						img->data[img_offset+0] = orig.buf[p+2];
					}
				} else {
					img->data[img_offset+2] = (char)(fill >> 16);
					img->data[img_offset+1] = (char)(fill >> 8);
					img->data[img_offset+0] = (char)(fill);
				}
			}
		}

		pthread_barrier_wait(&screen_out_barrier);
	}
}


void
project() {
	if (!orig_loaded) return;

    pthread_barrier_wait(&screen_in_barrier);
    pthread_barrier_wait(&screen_out_barrier);
	updatetitle();
	draw();
}

void
updatetitle() {
	char* title;
	int lat, lon = (int)(round(longitude_offset * 360)) % 360;

	if (lon < 0) lon = 360 + lon;
	if (SPHERE3D_SELECTED) {
		lat = round(latitude_offset * -90 / PI_HALF);
		if(asprintf(&title, "%s: %d° %d° %s", projections[projectionId].name, lon, lat, flipped ? "F" : "") < 0)
			die("Unable to create window title");

	} else {
		if(asprintf(&title, "%s: %d° %s", projections[projectionId].name, lon, flipped ? "F" : "") < 0)
			die("Unable to create window title");
	}

	XStoreName(display, win, title);
	free(title);
}

void
open_window() {
	XSetWindowAttributes attr;

	if ((display = XOpenDisplay(NULL)) == NULL)
		die("Cannot open display\n");

	screenId = DefaultScreen(display);

	if (!(opt_embed && (root = strtol(opt_embed, NULL, 0))))
		root = RootWindow(display, screenId);

	attr.event_mask = ExposureMask | StructureNotifyMask | KeyPressMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask;
	attr.background_pixel = fill;
	attr.border_pixel = BlackPixel(display, screenId);

	win = XCreateWindow( display, root,
			200, 200,
			800, 500,
			1,
			CopyFromParent,
			InputOutput,
			CopyFromParent,
			CWBackPixel | CWBorderPixel | CWEventMask,
			&attr);

	protocols[WmDeleteWin] = XInternAtom(display, "WM_DELETE_WINDOW", False);

	protocols[XdndEnter] =    XInternAtom(display, "XdndEnter", False);
	protocols[XdndPosition] = XInternAtom(display, "XdndPosition", False);
	protocols[XdndDrop] =     XInternAtom(display, "XdndDrop", False);
	protocols[XdndLeave] =    XInternAtom(display, "XdndLeave", False);

	mime_type = XInternAtom(display, "text/uri-list", False);
	XdndStatus =     XInternAtom(display, "XdndStatus", False);
	XdndSelection =  XInternAtom(display, "XdndSelection", False);
	XdndActionCopy = XInternAtom(display, "XdndActionCopy", False);
	XdndTypeList =   XInternAtom(display, "XdndTypeList", False);
	XdndFinished =   XInternAtom(display, "XdndFinished", False);

	XSetWMProtocols(display, win, protocols, ProtocolsCount);

	Atom version = 5;
	Atom dnd_aware = XInternAtom(display, "XdndAware", False);
	XChangeProperty(display, win, dnd_aware, XA_ATOM, 32, PropModeReplace, (unsigned char*) &version, 1);

	char *window_name = "SProjector Drop image here";
	XTextProperty wm_name;
	if (XStringListToTextProperty(&window_name, 1, &wm_name) == 0)
		die("Can't allocate wm name.");

	XWMHints wm_hint = {.flags = InputHint, .input = True};
	XClassHint class_hint = {.res_name = "debug", .res_class = "debug"};
	XSetWMProperties(display, win, &wm_name, NULL, NULL, 0, NULL, &wm_hint, &class_hint);

	gc = XCreateGC(display, win, line_style_mask, &line_style);
	XMapWindow(display, win);
}

void
sendXdndStatus(Atom proposed_action) {
	XEvent message;
	memset(&message, 0, sizeof(message));
	message.xclient.type = ClientMessage;
	message.xclient.display = display;
	message.xclient.window = sender;
	message.xclient.message_type = XdndStatus;
	message.xclient.format = 32;
	message.xclient.data.l[0] = win;

	// Sets accept and want position flags
	message.xclient.data.l[1] = 1;
	message.xclient.data.l[2] = 0;
	message.xclient.data.l[3] = 0;

	// Specify action we accept
	message.xclient.data.l[4] = proposed_action;

	if (XSendEvent(display, sender, False, 0, &message) == 0)
		die("XSendEvent");
}

void
sendXdndFinished() {
	XEvent message;
	memset(&message, 0, sizeof(message));
	message.xclient.type = ClientMessage;
	message.xclient.display = display;
	message.xclient.window = sender;
	message.xclient.message_type = XdndFinished;
	message.xclient.format = 32;
	message.xclient.data.l[0] = win;
	message.xclient.data.l[1] = 1;
	message.xclient.data.l[2] = XdndActionCopy;

	if (XSendEvent(display, sender, False, 0, &message) == 0)
		die("Unable to send XdndFinished Event");
}

void
readfromselection(Atom property) {
	Atom actual_type;
	unsigned long size, length;
	unsigned char *data = NULL;
	char *text_data, *substring;
	int actual_format;

	if (XGetWindowProperty(display, win, property, 0, PATH_MAX, 0, mime_type,
			&actual_type, &actual_format, &length, &size, &data) != 0) return;

	text_data = ecalloc(length + 1, sizeof(char));
	memcpy(text_data, data, length);
	text_data[length] = '\0';
	XFree(data);

	substring = (strstr(text_data, "file://")) == NULL ? text_data : text_data + 7; /* cut initial part */
	/* cut the end */
	if (substring[strlen(substring)-2] == '\r' && substring[strlen(substring)-1] == '\n')
		substring[strlen(substring)-2] = '\0';

	free(dropped_filename);
	dropped_filename = urldecode(substring);
	free(text_data);
	loadorig(1);
}

int
hasmime(Window w) {
	Atom actual_type, *list;
	int format;
	unsigned long count, data_len;
	unsigned char *data = NULL;
	if (XGetWindowProperty(display, w, XdndTypeList, 0, 1024, 0, AnyPropertyType,
				&actual_type, &format, &count, &data_len, &data) == 0) {
		if (actual_type != None) {
			list = (Atom *)data;
			for (int i = 0; i < count; ++i) {
				if (list[i] == mime_type) {
					return 1;
				}
			}
			XFree(data);
		}
	}
	return 0;
}

void
centeratpoint(int mx, int my) {
	double x, y, xo, yo;
	mx -= (winW - img->width) / 2;
	my -=(winH - img->height) / 2;

	if(mx < 0 || mx > img->width || my < 0 || my > img->height) return;

	x = (double)mx / img->width;
	y = (double)my / img->height;

	if (projections[projectionId].func(x, y, &xo, &yo, latitude_offset)) {
		xo += longitude_offset;
		xo -= (int)xo;
		longitude_offset = xo - 0.5;
		latitude_offset = (yo - 0.5) * M_PI;
		latitude_offset = CLAMP(latitude_offset, -PI_HALF, PI_HALF);

		new_uv(img->width, img->height, 0);
		project();
	}
}

void
run() {
	XEvent e;
	char buffer[INOTIFY_BUFFER_SIZE];

	while (1) {
		if (poll (fds, 1, 0) < 0) {
			die("Unable to poll");
		}


		if (fds[0].revents & POLLIN) {
			fds[0].revents = 0;

			if (read(fds[0].fd, buffer, INOTIFY_BUFFER_SIZE) > 0) {
				fprintf(stderr, "Reloading image\n");
				loadorig(0);
				XSync(display, 0);
			}
		}

		while (XPending(display) > 0) {
			XNextEvent(display, &e);
			switch(e.type)
			{
				case Expose:
					draw();
					break;
				case ConfigureNotify:
					winW = e.xconfigure.width;
					winH = e.xconfigure.height;
					createimg();
					break;
				case KeyPress:
					keypress(&e.xkey);
					break;
				case ButtonPress:
					if (orig_loaded && e.xbutton.button == Button1) {
						if (CLEANMASK(e.xbutton.state) == CLEANMASK(ShiftMask)) {
							centeratpoint(e.xbutton.x, e.xbutton.y);
						} else {
							drag = 1;
							mousex = e.xbutton.x;
							mark_lon = longitude_offset;
						}
					}
					break;
				case ButtonRelease:
					drag = 0;
					break;
				case MotionNotify:
					if (!drag) continue;

					if ((e.xmotion.time - last_mouse_drag) <= (1000 / 28)) continue;
					last_mouse_drag = e.xmotion.time;

					double dx = e.xmotion.x - mousex;
					longitude_offset = mark_lon - (dx / winW);
					project();
					break;
				case SelectionNotify:
					readfromselection(e.xselection.property);
					sendXdndFinished();
					break;
				case ClientMessage:
					if (e.xclient.data.l[0] == protocols[WmDeleteWin])
						return;

					if (e.xclient.message_type == protocols[XdndEnter]) {
						status_sent = 0;
						sender = -1;
						if (e.xclient.data.l[1] & 0x1 && hasmime(e.xclient.data.l[0])) {
							sender = e.xclient.data.l[0];
						} else if (e.xclient.data.l[2] == mime_type || e.xclient.data.l[3] == mime_type || e.xclient.data.l[4] == mime_type ) {
							sender = e.xclient.data.l[0];
						}
					} else if (e.xclient.message_type == protocols[XdndPosition]) {
						if (e.xclient.data.l[0] == sender && !status_sent) {
							sendXdndStatus(e.xclient.data.l[4]);
							status_sent = 1;
						}
					} else if (e.xclient.message_type == protocols[XdndDrop]) {
						XConvertSelection(display, XdndSelection, mime_type, mime_type, win,
								(Time)e.xclient.data.l[2]);
					} else if (e.xclient.message_type == protocols[XdndLeave]) {
						sender = -1;
					}

					break;
			}
		}
	}
}

void
draw() {
	int xmargin, ymargin;

	if (!orig_loaded) {
		XDrawRectangle(display, win, gc,
				drop_rect_padding,
				drop_rect_padding,
				winW - drop_rect_padding*2,
				winH - drop_rect_padding*2);
		return;
	}

	xmargin = (winW - img->width) / 2;
	ymargin = (winH - img->height) / 2;

	XPutImage(display, win, gc, img, 0, 0, xmargin, ymargin, img->width, img->height);

	if (xmargin > 0) {
		XClearArea(display, win, 0,            0, xmargin, winH, 0);
		XClearArea(display, win, winW-xmargin-1, 0, xmargin, winH, 0);
	}
	if (ymargin > 0) {
		XClearArea(display, win, 0, 0,            winW, ymargin, 0);
		XClearArea(display, win, 0, winH-ymargin-1, winW, ymargin, 0);
	}
	XFlush(display);
}

void
keypress(XKeyEvent *e) {
	unsigned int i;
	KeySym sym;

	sym = XkbKeycodeToKeysym(display, (KeyCode)e->keycode, 0, 0);
	for (i = 0; i < LEN(keys); i++)
		if (sym == keys[i].keysym && keys[i].func &&
			(CLEANMASK(keys[i].mod) == CLEANMASK(e->state)))
			keys[i].func(&(keys[i].arg));
}

void
close_window() {
	XEvent event;
	event.xclient.type = ClientMessage;
	event.xclient.window = win;
	event.xclient.message_type = XInternAtom(display, "WM_PROTOCOLS", 1);
	event.xclient.format = 32;
	event.xclient.data.l[0] = protocols[WmDeleteWin];
	event.xclient.data.l[1] = CurrentTime;
	XSendEvent(display, win, 0, NoEventMask, &event);
}

void
enablegrid() {
	grid_enabled = !grid_enabled;
	project();
}

void
flip() {
	flipped = !flipped;
	project();
}

void
offset_longitude(const Arg *arg) {
	longitude_offset += arg->f;
	project();
}

void
offset_latitude(const Arg *arg) {
	if (!orig_loaded) return;
	latitude_offset += arg->f;
	latitude_offset = CLAMP(latitude_offset, -M_PI/2, M_PI/2);
	new_uv(img->width, img->height, 0);
	project();
}

void
exportimg() {
	FILE *p, *file;
	char *cmd, size_val[32], *height_val = NULL, *content;
	double aspect;
	int width , height, sector = 0, i;

	if (asprintf(&cmd, filepicker_cmd, export_filename) < 0)
		die("Unable to format 'Filepicker Cmd'");
	if(!(p = popen(cmd, "r"))) return;
	free(cmd);
	while (fgets(export_filename, sizeof(export_filename), p) != NULL);
	if(pclose(p) || strlen(export_filename) < 2) return;
	export_filename[strlen(export_filename)-1] = '\0';

	if (asprintf(&cmd, width_cmd, win) < 0)
		die("Unable to format 'Width Cmd'");
	if(!(p = popen(cmd, "r"))) return;
	free(cmd);

	while (fgets(size_val, sizeof(size_val), p) != NULL);
	if(pclose(p)) return;

	for (i = 0; size_val[i] != '\0'; i++) {
		if (sector == 0 && isnum(size_val[i])) {
			sector++;
		} else if (sector == 1 && !isnum(size_val[i])) {
			sector++;
		} else if (sector == 2 && isnum(size_val[i])) {
			height_val = &(size_val[i]);
			break;
		}
	}

	width = atoi(size_val);
	if (height_val != NULL) {
		height = atoi(height_val);
	} else {
		aspect = projections[projectionId].aspect;
		height = width / (aspect < 0 ? export_aspect : aspect);
	}

	if (width <= 2 || height <= 2) {
		NotifyNotification *notif;
		notify_init("sprojector");
		notif = notify_notification_new("Saving Aborted", "Image must be at least 2x2", NULL);
		notify_notification_show(notif, NULL);
		return;
	}

	file = fopen(export_filename, "w");

	char png_char = export_filename[strlen(export_filename)-3]; /* Check if file name ends with p(ng) or P(NG)*/
	if (png_char == 'p' || png_char == 'P') {
		export_mode = ExportPng;
		prepare_exportable(width, height);
		writepng(file, &exportable);
	} else {
		export_mode = ExportJpg;
		prepare_exportable(width, height);
		writejpg(file, &exportable);
	}
	export_mode = ForScreen;
	fclose(file);

	if (asprintf(&content, "%s\n  %dx%d", export_filename, width, height) < 0)
		die("Unable to compose message");

	NotifyNotification *notif;
	notify_init("sprojector");
	notif = notify_notification_new("Image saved", content, NULL);
	notify_notification_show(notif, NULL);
	fprintf(stderr, "Write: %s\n", content);
	free(content);
}

void
projectionmenu() {
	char winid_arg[32], selected_arg[32], selection_name[64];
	char *list, *name;
	char ch;
	int i, j, offset = 0, list_len = 0, pid, status;

	int inpipe[2];
	int outpipe[2];

	for(i = 0; i < LEN(projections); i++) {
		list_len += strlen(projections[i].name) + 1;
	}

	list = ecalloc(list_len, sizeof(char));
	for(i = 0; i < LEN(projections); i++) {
		name = projections[i].name;

		for(j = 0; j < strlen(name); j++) {
			list[offset++] = name[j];
		}
		list[offset++] = '\n';
	}

	if (pipe(inpipe) < 0)
		die("sprojector: Unable to create pipe:");
	if (pipe(outpipe) < 0)
		die("sprojector: Unable to create pipe:");

	pid = fork();
	switch(pid) {
		case -1:
			die("sprojector: Unable to fork:");
		case 0:

			dup2(inpipe[rside], fileno(stdin));
			dup2(outpipe[wside], fileno(stdout));
			close(outpipe[wside]);
			close(outpipe[rside]);
			close(inpipe[wside]);
			close(inpipe[rside]);

			snprintf(selected_arg, sizeof(selected_arg), "%d", projectionId);
			snprintf(winid_arg, sizeof(winid_arg), "%d", (int)win);
			execlp(selector_cmd, NULL);
			exit(1);
			break;
		default:
			write(inpipe[wside], list, strlen(list));
			close(inpipe[wside]);
			waitpid(pid, &status, 0);

			if (status)
				break;

			for(i = 0; i < LEN(selection_name); i++) {
				if(read(outpipe[rside], &ch, sizeof(ch)) <= 0) {
					die("sprojector: Unable to read from pipe:");
				}
				if (ch == '\n') {
					selection_name[i] = '\0';
					break;
				} else {
					selection_name[i] = ch;
				}
			}
			close(outpipe[rside]);

			for (i = 0; i < LEN(projections); i++) {
				if (!strcmp(selection_name, projections[i].name)) {
					projectionId = i;

					createimg();
					break;
				}
			}
			break;
	}
	free(list);
}

void
usage(void) {
	die("usage: %s [file]", argv0);
}

int
main(int argc, char **argv) {
	ARGBEGIN {
		case 'v':
			fprintf(stderr, "sprojector-"VERSION"\n");
			return 0;
		case 'e':
			opt_embed = EARGF(usage());
			break;
		case 'i':
			filename = EARGF(usage());
			break;
		default:
			usage();
	} ARGEND

	if (!filename)
		filename = argv[0];

	fds[0].events = POLLIN;
	fds[0].fd = inotify_init();
	monitor = inotify_add_watch(fds[0].fd, filename, IN_CLOSE_WRITE);

	size_t thread_ids[MAX(UV_THREADS, SCREEN_THREADS)];
	for (size_t i = 0; i < MAX(UV_THREADS, SCREEN_THREADS); i++) {
		thread_ids[i] = i;
	}

	pthread_t uv_threads[UV_THREADS];
	pthread_t screen_threads[SCREEN_THREADS];

    pthread_barrier_init(&uv_in_barrier,  NULL, UV_THREADS+1);
    pthread_barrier_init(&uv_out_barrier, NULL, UV_THREADS+1);
    pthread_barrier_init(&screen_in_barrier,  NULL, SCREEN_THREADS+1);
    pthread_barrier_init(&screen_out_barrier, NULL, SCREEN_THREADS+1);

	for(size_t i = 0; i < UV_THREADS; i++) {
		pthread_create(&uv_threads[i], NULL, new_uv_p, &thread_ids[i]);
	}
	for(size_t i = 0; i < SCREEN_THREADS; i++) {
		pthread_create(&screen_threads[i], NULL, project_p, &thread_ids[i]);
	}

	open_window();
	loadorig(0);
	run();

	XDestroyWindow(display, win);
	XSync(display, False);
	XCloseDisplay(display);
	inotify_rm_watch (fds[0].fd, monitor);
	close (fds[0].fd);

	for(size_t i = 0; i < UV_THREADS; i++) {
		pthread_detach(uv_threads[i]);
	}
	for(size_t i = 0; i < SCREEN_THREADS; i++) {
		pthread_detach(screen_threads[i]);
	}

	stop_threads=1;
	pthread_barrier_wait(&uv_in_barrier);
	pthread_barrier_wait(&screen_in_barrier);

    pthread_barrier_destroy(&uv_in_barrier);
    pthread_barrier_destroy(&uv_out_barrier);
    pthread_barrier_destroy(&screen_in_barrier);
    pthread_barrier_destroy(&screen_out_barrier);

	return 0;
}
