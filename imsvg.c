#include "imsvg.h"
#include "math.h"

#define DEFAULT_CLASS_AND_ID \
			!meridians && deg == 0 ? "equator" : "gridline", \
			meridians ? "Meridian" : deg == 0 ? "Equator" : "Parallel", deg \

static FILE *f;
static int smooth_path = 0, outline = 0;
static double last_point_x, last_point_y;
int meridians = 0;

void
set_grid_mode(int mode) {
	meridians = mode;
}

void
start_svg(char *path, char *image_src, int width, int height) {
	outline = 0;
	smooth_path = 0;
	meridians = 0;
	f = fopen(path, "w");
	fprintf(f,
			"<?xml version='1.0' encoding='UTF-8' standalone='no'?>\n"
			"<svg version='1.1' id='svg1'\n"
			"\twidth='%d' height='%d'\n"
			"\tviewBox='0 0 %d %d'\n"
			"\txmlns='http://www.w3.org/2000/svg'\n"
			"\txmlns:svg='http://www.w3.org/2000/svg'>\n"
			"\t<style>\n"
			"\t\t.gridline { fill:none; stroke:black; stroke-width:1; stroke-dasharray:6,4; }\n"
			"\t\t.outline { fill:none; stroke:red; stroke-width:4; }\n"
			"\t\t.equator { fill:none; stroke:orange; stroke-width:2; }\n"
			"\t</style>\n"
			"\t<g id='Map'>\n"
			"\t\t<image width='%d' height='%d' href='%s' id='map' />\n"
			"\t</g>\n"
			"\t<g id='Grid'>\n",
			width, height, width, height, width, height, image_src);
}
void
end_svg() {
	fprintf(f,
			"\t</g>\n"
			"</svg>");
	fclose(f);
}


void
arc(int deg, double x0, double y0, double x1, double y1, double radius, int flip) {
	double dx = x1-x0;
	double dy = y1-y0;
	double radius_along_line = sqrt(dx*dx+dy*dy) / 2;
	double angle = atan2(dx, dy) / M_PI * 180;
	fprintf(f, "\t\t<path class='%s' id='%s %d' d='M %f %f A %f %f %f 0 %d %f %f'/>\n",
			DEFAULT_CLASS_AND_ID,
			x0, y0,
			radius,
			radius_along_line,
			angle,
			flip, x1, y1
			);
}
void
line(int deg, double x0, double y0, double x1, double y1) {
	fprintf(f, "\t\t<line class='%s' id='%s %d' x1='%f' y1='%f' x2='%f' y2='%f' />\n",
			DEFAULT_CLASS_AND_ID, x0, y0, x1, y1);
}


void
path(int deg, double x, double y) {
	fprintf(f, "\t\t<path class='%s' id='%s %d' d='M %f,%f ", DEFAULT_CLASS_AND_ID, x, y);
	last_point_x = x;
	last_point_y = y;
}
void
add_point(double x, double y) {
	fprintf(f, "%s%f,%f ", smooth_path ? "" : "L ", x, y);
	last_point_x = x;
	last_point_y = y;
}
void
add_arc(double x, double y, double radius, int flip) {
	double dx = x-last_point_x;
	double dy = y-last_point_y;
	double radius_along_line = sqrt(dx*dx+dy*dy) / 2;
	double angle = atan2(dx, dy) / M_PI * 180;
	fprintf(f, "A %f %f %f 0 %d %f %f ",
			radius,
			radius_along_line,
			angle,
			flip, x, y
			);

	last_point_x = x;
	last_point_y = y;
}
void
set_smooth(int enabled) {
	smooth_path = enabled;
	if(enabled) {
		fprintf(f, "C ");
	}
}

void
add_sinevawe(double xoffset, double h) {
	double c1 = .3642124232*h/2;
	double c2 = (1-0.682106)*h/2;

	fprintf(f, "%f,%f %f,%f %f,%f %f,%f %f,%f %f,%f ",
			last_point_x+xoffset/2, last_point_y + c2,
			last_point_x+xoffset, last_point_y + h/2-c1,
			last_point_x+xoffset, last_point_y + h/2,
			last_point_x+xoffset, last_point_y + h/2+c1,
			last_point_x+xoffset/2, last_point_y + h-c2,
			last_point_x, last_point_y + h);
	last_point_y += h;
}

void
end_path(int close) {
	smooth_path = 0;
	fprintf(f, " %s'/>\n", close ? "Z" : "");
}



void
outline_path(double x, double y) {
	fprintf(f,
			"\t</g>\n"
			"\t<g id='Outline'>\n"
			 "\t\t<path class='outline' id='frame' d='M %f,%f ", x, y);
	last_point_x = x;
	last_point_y = y;
}
void
outline_frame(double w, double h) {
	fprintf(f,
			"\t</g>\n"
			"\t<g id='Outline'>\n"
			"\t\t<rect class='outline' id='frame' width='%f' height='%f' />\n",
			w, h);
}
void
outline_round_frame(double w, double h) {
	fprintf(f,
			"\t</g>\n"
			"\t<g id='Outline'>\n"
			"\t\t<ellipse class='outline' id='frame' cx='%f' rx='%f' cy='%f' ry='%f' />\n",
			w/2, w/2, h/2, h/2);
}
void
outline_polygon(int count, ...) {
	va_list coords;
	fprintf(f,
			"\t</g>\n"
			"\t<g id='Outline'>\n"
			"\t\t<path class='outline' id='frame' d='M ");

	va_start(coords, count);
	for (int i = 0; i < count; i++) {
		double x = va_arg(coords, double);
		double y = va_arg(coords, double);
		fprintf(f, "%f,%f ", x, y);
	}
	va_end(coords);
	fprintf(f, " Z'/>\n");
}
