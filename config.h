#define UV_THREADS 64
#define SCREEN_THREADS 16
#define GRID_STEP 10

//char *filepicker_cmd = "yad --file --save --mouse --width=900 --height=700 --title='Select' --image-filter --confirm-overwrite";
char *filepicker_cmd =
"zenity --title=" S_EXPORT_TITLE
" --file-selection --save "
"--file-filter='Supported| *.png *.jp*g *.PNG *.JP*G' "
"--file-filter='All|*' "
"--filename='%s'";

char *width_cmd = "echo '' | dmenu -w %ld -p " S_EXPORT_SIZE_SELECTOR;

#define selector_cmd "dmenu", "dmenu", "-p", "Projection type:", "-l", "12", "-n", selected_arg, "-w", winid_arg
//#define selector_cmd "dmenu", "dmenu", "-p", "Projection type:", "-l", "40", "-w", winid_arg

static const unsigned long int fill = 0x657b83;
static const unsigned long int grid_color = 0xfdf6e3;
static const unsigned long int jpg_fill = 0xff00ff;
static const unsigned long int png_fill = 0x00000000;

static const int drop_rect_padding = 80;
static const int line_style_mask = GCLineWidth | GCForeground | GCLineStyle | GCDashList;
static XGCValues line_style = {
	.line_width = 3,
	.line_style = LineOnOffDash,
	.foreground = 0xfdf6e3,
	.dashes     = 10,
};

double export_aspect = 1920.0 / 1080.0;

int sphere3d_id = 1;
static Projection projections[] = {
	/* name 				aspect-ratio	function */
	{ S_PROJECTION_HAMMER, 		2,	 		hammer, hammer_g},
	{ "Sphere 3D",			1,	 			sphere3d, sphere3d_g},
	{ "Equirectangular",	-1,	 			equirectangular, equirectangular_g},
	{ "Cylindrical",		-1,	 			lambert_cyl, lambert_cyl_g},
	{ "Mercator",			1.65,	 		mercator, mercator_g},

	{ S_PROJECTION_MOLLWEIDE, 	2,	 		mollweide, mollweide_g},
	{ S_PROJECTION_ECKERT2,		-1,	 		eckert2, eckert2_g},
	{ S_PROJECTION_ECKERT4,		2,	 		eckert4, eckert4_g},
	{ S_PROJECTION_ECKERT6,		-1,	 		eckert6, eckert6_g},
	{ S_PROJECTION_SINUSOIDAL,	-1,	 			sinusoidal, sinusoidal_g},

	{ S_PROJECTION_WERNER,		WERNER_ASPECT,	werner, werner_g},
	{ "Azimut", 			1,	 				azimut, azimut_g},
	{ "Van der Grinten",	1,	 				vandergrinten, vandergrinten_g},
	{ "American polyconic",	-1,	 				american_polyconic, american_polyconic_g},
	{ S_PROJECTION_CASSINI, 	0.5,	 		cassini, cassini_g},
	{ "Wagner 6",			2,	 			wagner6, wagner6_g},
	{ "Collignon",				-1,	 			collingnon, collingnon_g},
};

#define LAT_DEG (PI_HALF/90.0)
#define LON_DEG (1.0/360.0)

static Key keys[] = {
	/* modifier		key			function	argument */
	{ 0,			XK_w,		exportimg,			{0} },
	{ 0,			XK_p,		projectionmenu,		{0} },
	{ 0,			XK_q,		close_window,		{0} },
	{ 0,			XK_g,		enablegrid,			{0} },
	{ 0,			XK_s,		writesvg,			{0} },
	{ 0,			XK_minus,	flip,				{0} },

	{ ShiftMask,	XK_h,		offset_longitude,	{.f = -LON_DEG} },
	{ ShiftMask,	XK_l,		offset_longitude,	{.f = +LON_DEG} },
	{ 0,			XK_h,		offset_longitude,	{.f = -LON_DEG*10} },
	{ 0,			XK_l,		offset_longitude,	{.f = +LON_DEG*10} },
	{ ShiftMask,	XK_Left,	offset_longitude,	{.f = -LON_DEG} },
	{ ShiftMask,	XK_Right,	offset_longitude,	{.f = +LON_DEG} },
	{ 0,			XK_Left,	offset_longitude,	{.f = -LON_DEG*10} },
	{ 0,			XK_Right,	offset_longitude,	{.f = +LON_DEG*10} },

	{ 0,			XK_Up,		offset_latitude,	{.f = -LAT_DEG*10} },
	{ 0,			XK_Down,	offset_latitude,	{.f = +LAT_DEG*10} },
	{ ShiftMask,	XK_Up,		offset_latitude,	{.f = -LAT_DEG} },
	{ ShiftMask,	XK_Down,	offset_latitude,	{.f = +LAT_DEG} },
	{ 0,			XK_k,		offset_latitude,	{.f = -LAT_DEG*10} },
	{ 0,			XK_j,		offset_latitude,	{.f = +LAT_DEG*10} },
	{ ShiftMask,	XK_k,		offset_latitude,	{.f = -LAT_DEG} },
	{ ShiftMask,	XK_j,		offset_latitude,	{.f = +LAT_DEG} },
};

/*
static Button buttons[] = {
	// event mask      button          function        argument
	{ 0,              Button1,        setlayout,      {0} },
};T
*/
