include config.mk

SRC = sprojector.c util.c urldecode.c imsvg.c
OBJ = $(SRC:.c=.o)

all: options sprojector

options:
	@echo sprojector build options:
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CC       = $(CC)"

.c.o:
	$(CC) -c $(CFLAGS) $<

config.h:
	cp config.def.h $@

$(OBJ): config.h projections.h util.h arg.h urldecode.h config.mk

sprojector: ${OBJ}
	$(CC) -o $@ ${OBJ} $(LDFLAGS)

clean:
	rm -f sprojector $(OBJ)

.PHONY: all options clean
