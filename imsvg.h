#include <stdio.h>
#include <stdarg.h>


#define MERIDIANS set_grid_mode(1);
#define PARALLELS set_grid_mode(0);

void set_grid_mode(int mode);
void start_svg(char *path, char *image_src, int width, int height);
void end_svg();

void arc(int deg, double x0, double y0, double x1, double y1, double radius, int flip);
void line(int deg, double x0, double y0, double x1, double y1);
void path(int deg, double x, double y);
void add_point(double x, double y);
void add_arc(double x, double y, double radius, int flip);
void set_smooth(int enabled);
void add_sinevawe(double xoffset, double h);
void end_path(int close);
void outline_path(double x, double y);
void outline_frame(double w, double h);
void outline_round_frame(double w, double h);
void outline_polygon(int count, ...);
