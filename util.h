/* See LICENSE file for copyright and license details. */
/*
#define MAX(A, B)               ((A) > (B) ? (A) : (B))
#define MIN(A, B)               ((A) < (B) ? (A) : (B))
*/
#define BETWEEN(X, A, B)        ((A) <= (X) && (X) <= (B))

void die(const char *fmt, ...);
void *ecalloc(size_t nmemb, size_t size);

#undef reallocarray
void *reallocarray(void *, size_t, size_t);
void *ereallocarray(void *optr, size_t nmemb, size_t size);
